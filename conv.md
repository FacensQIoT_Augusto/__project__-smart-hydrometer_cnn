# Redes convolucionais

## Convolução

https://youtu.be/sAPg-qaT0b4

https://youtu.be/iDH3LCZwL5M - Com cores

## Max pooling

https://youtu.be/o_DJ-FO6dw0

## Key-Words:

### Recap

We just learned about convolutions and max pooling.

A convolution is the process of applying a filter (“kernel”) to an image. Max pooling is the process of reducing the size of the image through downsampling.

As you will see in the following Colab notebook, convolutional layers can be added to the neural network model using the Conv2D layer type in Keras. This layer is similar to the Dense layer, and has weights and biases that need to be tuned to the right values. The Conv2D layer also has kernels (filters) whose values need to be tuned as well. So, in a Conv2D layer the values inside the filter matrix are the variables that get tuned in order to produce the right output.

Here are some of terms that were introduced in this lesson:

- **CNNs:** Convolutional neural network. That is, a network which has at least one convolutional layer. A typical CNN also includes other types of layers, such as pooling layers and dense layers.

- **Convolution:** The process of applying a kernel (filter) to an image

- **Kernel / filter:** A matrix which is smaller than the input, used to transform the input into chunks
    
- **Padding:** Adding pixels of some value, usually 0, around the input image
    
- **Pooling:** The process of reducing the size of an image through downsampling.There are several types of pooling layers. For example, average pooling converts many values into a single value by taking the average. However, maxpooling is the most common.
    
- **Maxpooling:** A pooling process in which many values are converted into a single value by taking the maximum value from among them.
    
- **Stride:** the number of pixels to slide the kernel (filter) across the image.
    
- **Downsampling:** The act of reducing the size of an image

https://towardsdatascience.com/a-comprehensive-guide-to-convolutional-neural-networks-the-eli5-way-3bd2b1164a53

### Possibilidades de trabalhar com classes binárias:

Another popular approach when working with binary classification problems, is to use a classifier that consists of a **Dense layer with 1 output unit and a sigmoid activation function, as seen below:**     

> tf.keras.layers.Dense(1, activation='sigmoid')

Either of these two options will work well in a binary classification problem. However, you should keep in mind, that if you decide to use a sigmoid activation function in your classifier, you will also have to **change the loss parameter in the model.compile() method**, from **'sparse_categorical_crossentropy'** to **'binary_crossentropy'**, as shown below:

> model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])


## Validation

https://youtu.be/efH2YKBeTm0

## Data augmentation

https://youtu.be/Qgd7maIVytI - Cria imagens artificiais

## 6 Truques para evitar overfitting

https://hackernoon.com/memorizing-is-not-learning-6-tricks-to-prevent-overfitting-in-machine-learning-820b091dc42

## 

