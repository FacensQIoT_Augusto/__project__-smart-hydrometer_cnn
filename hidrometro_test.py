import cv2
import imutils
from datetime import datetime
from tensorflow.keras.models import load_model
import numpy as np

vobj = cv2.VideoCapture("out_f.avi")

# ROIS 30 x 56
D1 = ((251, 190), (281, 246))
D2 = ((307, 190), (337, 246))
D3 = ((361, 190), (391, 246))
D4 = ((416, 190), (446, 246))
D5 = ((469, 190), (499, 246))
D6 = ((522, 190), (552, 246))
rois = [D1, D2, D3, D4, D5, D6]
status = False

# Carrega o modelo
model = load_model('hydrometer_model.h5')

while True:   
    rois_list = [] 
    
    status, frame = vobj.read()

    # ROIS das regiões de interesse 
    for r in rois:
        rois_list.append(frame[r[0][1]:r[1][1], r[0][0]:r[1][0]])
    
    new_f = frame[D6[0][1]:D6[1][1], D6[0][0]:D6[1][0]]
    # Utiliza o modelo treinado para efetuar predições
    pred = model.predict(np.array(rois_list))
    pred = pred.argmax(axis = 1)


    for i, r in enumerate(rois):
        cv2.putText(frame, str(pred[i]), (int((r[1][0] - r[0][0]) / 2 + r[0][0] - 10), r[0][1] - 10), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.rectangle(frame, (r[0][0],r[0][1]), (r[1][0],r[1][1]), (255, 0, 0), 2)


    cv2.imshow('Predicted', new_f)
    print(pred)
    cv2.imshow("Output", frame) 

    key = cv2.waitKey(1) & 0xFF

    if key == ord("q") or status == False:
        break

vobj.release()
cv2.destroyAllWindows()
